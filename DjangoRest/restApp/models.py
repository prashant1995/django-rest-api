from django.db import models

class Employee(models.Model):
    firstName = models.CharField(max_length=30)
    lastName = models.CharField(max_length=30)
    empId = models.IntegerField()

    def __str__(self):
        return self.firstName
